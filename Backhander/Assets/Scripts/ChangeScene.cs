﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{

    AudioSource ball_sunk;
    private static int level = 0;

    // Start is called before the first frame update
    void Start()
    {
        ball_sunk = GetComponent<AudioSource>();

    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            ball_sunk.Play();
            Invoke("ChangeIt", 2.0f);
        }
    }

    void ChangeIt()
    {
        level++;
        SceneManager.LoadScene(level);
    }

    // Update is called once per frame
    void Update()
    {
        OnTriggerEnter(GetComponent<Collider>());        
    }
}
