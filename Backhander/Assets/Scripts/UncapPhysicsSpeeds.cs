﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UncapPhysicsSpeeds : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Rigidbody rb = GetComponent<Rigidbody>();

        if (rb == null)
        {
            Debug.LogError("No Rigidbody found on " + gameObject.name);
            return;
        }

        rb.maxAngularVelocity = Mathf.Infinity;

        Destroy(this);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
